libtext-flow-perl (0.01-3) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Jonathan Yu from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 28 Jun 2022 20:04:39 +0100

libtext-flow-perl (0.01-2.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Tue, 05 Jan 2021 16:09:03 +0100

libtext-flow-perl (0.01-2) unstable; urgency=low

  * Team upload.

  [ gregor herrmann ]
  * debian/rules: switch order of arguments to dh.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.
  * Switch to source format "3.0 (quilt)".
  * Add explicit build dependency on libmodule-build-perl.
  * Bump debhelper compatibility level to 9.
  * Drop quilt fragments from debian/*.
  * Refresh and forward fix-tests.patch.
  * Update license stanzas in debian/copyright.

 -- gregor herrmann <gregoa@debian.org>  Sun, 07 Jun 2015 23:32:26 +0200

libtext-flow-perl (0.01-1) unstable; urgency=low

  * Initial Release (Closes: #566908)

 -- Jonathan Yu <jawnsy@cpan.org>  Mon, 25 Jan 2010 18:36:42 -0500
